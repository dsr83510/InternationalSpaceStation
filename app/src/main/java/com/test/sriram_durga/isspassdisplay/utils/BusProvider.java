package com.test.sriram_durga.isspassdisplay.utils;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by sriram_durga on 12/24/17.
 * Provides an (a sigleton) event bus instance to the app.
 */

public class BusProvider {

    private static Bus bus;

    private BusProvider() {

    }

    public static Bus getBus() {
        if (bus == null) {
            bus = new Bus(ThreadEnforcer.ANY);
        }
        return bus;
    }

}
