package com.test.sriram_durga.isspassdisplay.viewControllers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.test.sriram_durga.isspassdisplay.R;
import com.test.sriram_durga.isspassdisplay.location.GPSLocationManager;
import com.test.sriram_durga.isspassdisplay.model.PassResponse;
import com.test.sriram_durga.isspassdisplay.presenter.FetchISSPassesContract;
import com.test.sriram_durga.isspassdisplay.presenter.FetchPassesPresenter;
import com.test.sriram_durga.isspassdisplay.utils.BusProvider;

import java.util.List;

/**
 * A simple activity class which displays a list of ISI passes, depending upon the present
 * GPS coordinate of the device. Note that the GPS coordinates might be mocked in certain
 * situations wherein the users might have not granted permissions or if testing on
 * the emulator.
 */

public class MainActivity extends AppCompatActivity implements FetchISSPassesContract.View {

    private FetchISSPassesContract.Presenter presenter;
    private ListView listView;
    private PassesListAdapter adapter;
    private GPSLocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = new GPSLocationManager();
        listView = findViewById(R.id.list_view); //TODO could have used a recycler view instead of a plain list view.
        presenter = new FetchPassesPresenter(this);
        adapter = new PassesListAdapter(null);
        listView.setAdapter(adapter);
        presenter.fetchRemoteData(locationManager.getCurrentLatitude(),locationManager.getCurrentLongitude());
    }


    @Override
    public void displayPasses(List<PassResponse> data) {
        adapter.setData(data);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void displayError(String msg) {
        //For now resorting to a Toast message instead of an alert dialog.
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.getBus().unregister(presenter);
    }
}
