package com.test.sriram_durga.isspassdisplay.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sriram_durga on 12/24/17. A simple GSON model class.
 */

public class Request {

    private int altitude;
    @SerializedName("datetime")
    private long dateTime;
    private double latitude;
    private double longitude;
    private int passes;

    public Request(int altitude, long dateTime, double latitude, double longitude, int passes) {
        this.altitude = altitude;
        this.dateTime = dateTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.passes = passes;
    }


    public int getAltitude() {
        return altitude;
    }

    public long getDateTime() {
        return dateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getPasses() {
        return passes;
    }
}
