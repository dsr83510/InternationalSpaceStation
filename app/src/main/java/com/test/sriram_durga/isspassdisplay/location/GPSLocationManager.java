package com.test.sriram_durga.isspassdisplay.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.test.sriram_durga.isspassdisplay.MainApplication;

/**
 * Created by sriram_durga on 12/24/17.
 * This class is responsible for feeding in the GPS coordinates of the device.
 * Note that the GPS coordinates might be mocked in certain
 * situations wherein the users might have not granted permissions or if testing on
 * the emulator.
 */

public class GPSLocationManager {

    private double currentLatitude;
    private double currentLongitude;
    private LocationManager locationManager;
    private static double MIN_LATITUDE = -80.0;
    private static double MAX_LATITUDE = 80.0;
    private static double MIN_LONGITUDE = -180.0;
    private static double MAX_LONGITUDE = 180.0;

    public GPSLocationManager() {
        Context applicationContext = MainApplication.getContext();
        // Get the location manager
        locationManager = (LocationManager) applicationContext.getSystemService (Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            setCurrentLatitude(MIN_LATITUDE + (Math.random() * (MAX_LATITUDE - MIN_LATITUDE))); //Default Mock values
            setCurrentLongitude(MIN_LONGITUDE + (Math.random() * (MAX_LONGITUDE - MIN_LONGITUDE))); //Default Mock values
            return;
        }
        else{
            Location location = locationManager.getLastKnownLocation(provider);
            if(location != null){
                setCurrentLatitude(location.getLatitude());
                setCurrentLongitude(location.getLongitude());
            }
        }
    }


    public double getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public double getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }
}

