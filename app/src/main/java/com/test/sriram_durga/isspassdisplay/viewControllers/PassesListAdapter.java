package com.test.sriram_durga.isspassdisplay.viewControllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.test.sriram_durga.isspassdisplay.MainApplication;
import com.test.sriram_durga.isspassdisplay.R;
import com.test.sriram_durga.isspassdisplay.model.PassResponse;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by sriram_durga on 12/24/17.
 * A simple list view adapter class which utilizes the View Holder design pattern
 * to make sure the list is efficient and hassle free while scrolling.
 */

public class PassesListAdapter extends BaseAdapter {

    private List<PassResponse> data;
    private LayoutInflater layoutInflater;

    public PassesListAdapter(List<PassResponse> data) {
        this.data = data;
        layoutInflater = (LayoutInflater) MainApplication.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(List<PassResponse> data) {
        this.data = data;
    }


    @Override
    public int getCount() {
        if (data != null)
            return data.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int i) {
        if (data != null && !data.isEmpty()) {
            return data.get(i);
        } else
            return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item, viewGroup, false);
            holder = new ViewHolder();
            holder.duration = convertView.findViewById(R.id.duration);
            holder.riseTime = convertView.findViewById(R.id.rise_time);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PassResponse passResponse = (PassResponse) getItem(position);

        if (passResponse != null) {
            holder.duration.setText(String.valueOf(passResponse.getDuration()));
            holder.riseTime.setText(DateFormat.getInstance().format(passResponse.getRiseTime()));
        }

        return convertView;

    }

    static class ViewHolder {
        private TextView duration;
        private TextView riseTime;
    }

}
