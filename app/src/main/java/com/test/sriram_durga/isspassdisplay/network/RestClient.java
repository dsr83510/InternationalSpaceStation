package com.test.sriram_durga.isspassdisplay.network;


import android.util.Log;

import com.test.sriram_durga.isspassdisplay.model.ServiceResponse;
import com.test.sriram_durga.isspassdisplay.network.api_services.GetPassesService;
import com.test.sriram_durga.isspassdisplay.network.events.NewDataEvent;
import com.test.sriram_durga.isspassdisplay.utils.BusProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This module handles the network requests and responses. Uses the retrofit library
 * to perform all HTTP transactions.
 */

public class RestClient {

    private Retrofit retrofit;
    private static final String BASE_URL = "http://api.open-notify.org/";
    private static final String TAG = RestClient.class.getSimpleName();
    private static final int NUMBER_OF_PASSES = 20;

    public RestClient() {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create());
        retrofit = builder.build();
    }

    public void fetchISSPassData(final double latitude, final double longitude){
        GetPassesService getPassesService = retrofit.create(GetPassesService.class);
        Call<ServiceResponse> call = getPassesService.getPasses(latitude,longitude, NUMBER_OF_PASSES);
        call.enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                if(response.code() == 200){
                    ServiceResponse serviceResponse = response.body();
                    if(serviceResponse != null){
                        NewDataEvent newDataEvent =  new NewDataEvent(serviceResponse.getResponse());
                        BusProvider.getBus().post(newDataEvent);
                    }
                }
                else{
                    Log.e(TAG,"Received unsuccessful response from service end point with an error code " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                Log.e(TAG,"Failed to receive a response from service end point due to " +  t.getMessage());
            }
        });


    }
}
