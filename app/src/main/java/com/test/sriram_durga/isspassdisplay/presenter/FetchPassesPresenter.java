package com.test.sriram_durga.isspassdisplay.presenter;

import com.squareup.otto.Subscribe;
import com.test.sriram_durga.isspassdisplay.network.RestClient;
import com.test.sriram_durga.isspassdisplay.network.events.NewDataEvent;
import com.test.sriram_durga.isspassdisplay.utils.BusProvider;

/**
 * Created by sriram_durga on 12/24/17.
 * A simple presenter which fetches remote data
 * and upon notification of event on the event bus informs the View to update.
 */

public class FetchPassesPresenter implements FetchISSPassesContract.Presenter {

    private RestClient restClient;
    private FetchISSPassesContract.View view;


    public FetchPassesPresenter(final FetchISSPassesContract.View view) {
        restClient = new RestClient();
        BusProvider.getBus().register(this);
        this.view =  view;
    }

    @Override
    public void fetchRemoteData(double latitude, double longitude) {
        restClient.fetchISSPassData(latitude,longitude);
    }

    @Subscribe
    public void onNewData(NewDataEvent newDataEvent){
        view.displayPasses(newDataEvent.getData());
    }
}
