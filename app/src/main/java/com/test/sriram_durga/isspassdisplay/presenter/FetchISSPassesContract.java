package com.test.sriram_durga.isspassdisplay.presenter;

import com.test.sriram_durga.isspassdisplay.model.PassResponse;

import java.util.List;

/**
 * Created by sriram_durga on 12/24/17. A very simple interface showcasing the
 * responsibilities of the View and Presenter.
 */

public interface FetchISSPassesContract {

    interface View {
        void displayPasses(final List<PassResponse> data);
        void displayError(final String msg);
    }

    interface Presenter {
        void fetchRemoteData(final double latitude, final double longitude);
    }

}
