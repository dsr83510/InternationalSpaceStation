package com.test.sriram_durga.isspassdisplay.network.events;

import com.test.sriram_durga.isspassdisplay.model.PassResponse;

import java.util.List;

/**
 * Created by sriram_durga on 12/24/17.
 */

public class NewDataEvent {

    private List<PassResponse> data;

    public NewDataEvent(List<PassResponse> data) {
        this.data = data;
    }

    public List<PassResponse> getData() {
        return data;
    }

    public void setData(List<PassResponse> data) {
        this.data = data;
    }
}
