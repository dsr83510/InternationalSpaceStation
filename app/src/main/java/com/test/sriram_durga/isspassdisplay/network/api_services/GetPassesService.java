package com.test.sriram_durga.isspassdisplay.network.api_services;

import com.test.sriram_durga.isspassdisplay.model.ServiceResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sriram_durga on 12/24/17.
 */

public interface GetPassesService {

    @GET("/iss-pass.json")
    Call<ServiceResponse> getPasses(@Query("lat") double latitude, @Query("lon") double longitude, @Query("n") int passes);
}


