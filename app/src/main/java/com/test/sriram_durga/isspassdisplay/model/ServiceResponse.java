package com.test.sriram_durga.isspassdisplay.model;

import java.util.List;

/**
 * Created by sriram_durga on 12/24/17.  A simple GSON model class.
 */

public class ServiceResponse {

    private String message;

    private Request request;

    private List<PassResponse> response;

    private boolean isSuccessful;


    public ServiceResponse(String message, Request request, List<PassResponse> response) {
        this.message = message;
        this.request = request;
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public Request getRequest() {
        return request;
    }

    public List<PassResponse> getResponse() {
        return response;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

}
