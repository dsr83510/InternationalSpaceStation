package com.test.sriram_durga.isspassdisplay.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sriram_durga on 12/24/17. A simple GSON model class.
 */

public class PassResponse {

    private int duration;
    @SerializedName("risetime")
    private long riseTime;

    public PassResponse(int duration, long riseTime) {
        this.duration = duration;
        this.riseTime = riseTime;
    }

    public int getDuration() {
        return duration;
    }

    public long getRiseTime() {
        return riseTime;
    }
}
