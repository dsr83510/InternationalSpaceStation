package com.test.sriram_durga.isspassdisplay;

import android.app.Application;
import android.content.Context;

/**
 * Created by sriram_durga on 12/24/17.
 */

public class MainApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext(){
        return context;
    }
}
