package com.test.sriram_durga.isspassdisplay;

import com.test.sriram_durga.isspassdisplay.model.PassResponse;
import com.test.sriram_durga.isspassdisplay.model.ServiceResponse;
import com.test.sriram_durga.isspassdisplay.network.events.NewDataEvent;
import com.test.sriram_durga.isspassdisplay.presenter.FetchISSPassesContract;
import com.test.sriram_durga.isspassdisplay.presenter.FetchPassesPresenter;
import com.test.sriram_durga.isspassdisplay.viewControllers.MainActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FetchPassesPresenter.class, PassResponse.class})
public class FetchPassesPresenterTest {


    private FetchPassesPresenter fetchPassesPresenter;
    private FetchISSPassesContract.View mockView;
    private boolean isCallBackTriggered = false;
    private static final int LATCH_TIMEOUT = 3000;


    @Before
    public void setup() {
        fetchPassesPresenter = PowerMockito.mock(FetchPassesPresenter.class);
        mockView = PowerMockito.mock(MainActivity.class);
        Whitebox.setInternalState(fetchPassesPresenter, "mView", mockView);
    }

    @Test
    public void testHandleUnSuccessfulNetworkResponse() {
        try {
            final CountDownLatch latch = new CountDownLatch(1);
            ServiceResponse mockNetworkResponse = PowerMockito.mock(ServiceResponse.class);
            PowerMockito.doReturn(false).when(mockNetworkResponse).isSuccessful();
            Mockito.doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    latch.countDown();
                    isCallBackTriggered = true;
                    return null;
                }
            }).when(mockView).displayError("Network Error, Please try again later.");

            //Create the event from the response.
            NewDataEvent newDataEvent = new NewDataEvent(mockNetworkResponse.getResponse());

            //Invoke the presenter response handler method.
            Mockito.doCallRealMethod().when(fetchPassesPresenter).onNewData(newDataEvent);
            fetchPassesPresenter.onNewData(newDataEvent);


            latch.await(LATCH_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Mockito answer did not get triggered", isCallBackTriggered);
        } catch (InterruptedException e) {
            Assert.assertTrue("Interrupted Exception", false);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.assertTrue("Exception Thrown", false);
        }
    }

    @Test
    public void testHandleSuccessfulNetworkResponse() {
        try {
            final CountDownLatch latch = new CountDownLatch(1);
            ServiceResponse mockNetworkResponse = PowerMockito.mock(ServiceResponse.class);
            PassResponse mockISSPassResponse = PowerMockito.mock(PassResponse.class);
            PowerMockito.doReturn(true).when(mockNetworkResponse).isSuccessful();
            PowerMockito.doReturn(mockISSPassResponse).when(mockNetworkResponse).getResponse();

            final List<PassResponse> passes = new ArrayList<>();
            PassResponse mockPassResponses = PowerMockito.mock(PassResponse.class);
            passes.add(mockPassResponses);
            PowerMockito.doReturn(passes).when(mockISSPassResponse).getRiseTime();
            Mockito.doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    latch.countDown();
                    List<Long> passTimes = invocation.getArgument(0);
                    Assert.assertTrue("The values are incorrect", passTimes != null && passTimes.size() == 1);
                    isCallBackTriggered = true;
                    return null;
                }
            }).when(mockView).displayPasses(passes);


            //Create the event from the response.
            NewDataEvent newDataEvent = new NewDataEvent(mockNetworkResponse.getResponse());

            //Invoke the presenter response handler method.
            Mockito.doCallRealMethod().when(fetchPassesPresenter).onNewData(newDataEvent);
            fetchPassesPresenter.onNewData(newDataEvent);

            latch.await(LATCH_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Mockito answer did not get triggered", isCallBackTriggered);
        } catch (InterruptedException e) {
            Assert.assertTrue("Interrupted exception", false);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.assertTrue("Exception Thrown", false);
        }
    }

    @Test
    public void testHandleNetworkResponseForNullNetworkResponse() {
        try {
            final CountDownLatch latch = new CountDownLatch(1);
            ServiceResponse mockNetworkResponse = null;
            Mockito.doAnswer(new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    latch.countDown();
                    isCallBackTriggered = true;
                    return null;
                }
            }).when(mockView).displayError("Null response from Network");

            //Create the event from the response.
            NewDataEvent newDataEvent = new NewDataEvent(mockNetworkResponse.getResponse());

            //Invoke the presenter response handler method.
            Mockito.doCallRealMethod().when(fetchPassesPresenter).onNewData(newDataEvent);
            fetchPassesPresenter.onNewData(newDataEvent);

            latch.await(LATCH_TIMEOUT, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Callback is not triggered", isCallBackTriggered);
        } catch (InterruptedException e) {
            Assert.assertTrue("Should not throw Interrupted exception", false);
        } catch (Exception e) {
            Assert.assertTrue("Should not throw exception", false);
        }

    }


}
